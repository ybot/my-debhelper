## mount build path in /data on the container
## run: podman run --rm -v .:/data my-debhelper
FROM docker.io/library/debian:bookworm
RUN apt-get update && apt-get -y install git debhelper config-package-dev
RUN apt-get clean && rm -rf /var/lib/apt/*
COPY build-deb /usr/bin
WORKDIR /data
ENTRYPOINT ["build-deb"]
